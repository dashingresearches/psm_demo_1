#ifndef Display_included
#define Display_included

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/


#include "dTypes.h"
using namespace dTypes;

#include <vcl.h>

#include <stdlib.h>
#include <time.h>

#include "Font.h"
#include "Font6x8.h"

#include "Shift_register.h"

#include "Videomemory.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class tDisplay {

  public:

  //////////////////////////////////////////////////////////////////////////////
  tDisplay( uint X_size, uint Display_Y_size, uint Line_buffer_Y_size);

  ///////////////////////////////////////////////////////////////////////////////////
  void Out_text (int arg_x0, int arg_y0,
                 int arg_x1, int arg_y1,
                 int arg_x_shift, int arg_y_shift,
                 unsigned char * argText,
                 TMemo * argDebug = 0);

  ///////////////////////////////////////////////////////////////////////////////////
  // ���������� ���������� ��������� ������ �� �����
  void Draw(TCanvas * Canvas);

  void Set_font(class tFont* argFont = (tFont*) &Font6x8);

  private:

  TMemo * Debug;

  friend class tMain_tab_process;

  // ��������� ����� ��� ������ �������
  tShift_register Symbol_buffer;

  // �������� �����
  vector< tShift_register > Line_buffer;

  tVideomemory Videomemory;

  uint x_max, y_max;

  //////////////////////////////////////////////////////////////////////////////
  const tFont * Current_font;

  //////////////////////////////////////////////////////////////////////////////
  // ������ ��� ������ ���������� ����� � �������� �����.
  u1x * Text, *Text_begin, *Text_end;

  // ������������ ��������, ����� ������� � ���������������
  int Start_line, End_line;

  // ��������� �������������� ����� � �������������� ���������� ����������
  int Left_shift, Current_shift, Current_byte;

  // ��������� �� ������ ������� Width ������ - ������� ���� ���������� �������
  const u1x * Symbol_array;

  // ������ ������� � ��������, ������ ������
  // � ������ ������ ����� ������ (����� ������������� �� 1 ����)
  int Width;
  int bytes_Width;
  int bytes_Width_after_shift;

  int Height;

  // ������ ����� ������ � ��������
  int Line_width;

  //////////////////////////////////////////////////////////////////////////////
  // ������ ��� ������ ��������� ������ � �����������
  int x_byte, Start_shift;

  int x0, x1, y0, y1, x_shift, y_shift;

  // ��������� ������ ������ ����������������, ������ ��������� ���� �����
  // ���������� �� ����� ���������, ������ ���������� ����� �������������,
  // ������������ x0, y0, �� ��� ������ ������������ ����������
  int x_rel, y_rel;

  int x_abs, y_abs;

  //////////////////////////////////////////////////////////////////////////////
  void Clear_line_buffer();

  //////////////////////////////////////////////////////////////////////////////
  void Out_symbol();

  //////////////////////////////////////////////////////////////////////////////
  void Out_text_block ();

  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  // ����� ��� ������������ �������� �����������, ��
  // Masks_array__right_for_line_buffer - ������������ ��� ������������ ��������� ������
  static const u1x Masks_array__left[8];
  static const u1x Masks_array__right[8];
  static const u1x Masks_array__left_for_line_buffer[8];
  static const u1x Masks_array__right_for_line_buffer[8];

  ///////////////////////////////////////////////////////////////////////////////////
  void Out_line_buffer_in_videomemory();

  ///////////////////////////////////////////////////////////////////////////////////
  bool Init_text_block ();

  ///////////////////////////////////////////////////////////////////////////////////
  void Control_processing ();

  

inline void Out_debug_data_for_Line_buffer(uchar Symbol)
{

  if(!Debug)

    return;

  Debug->Lines->Add(AnsiString("  ") );
  Debug->Lines->Add(AnsiString("Symbol: ") + AnsiString((char*)&Symbol,1));
  Debug->Lines->Add(AnsiString("Left_shift: ") + Left_shift );
  Debug->Lines->Add(AnsiString("Current_shift: ") + Current_shift );
  Debug->Lines->Add(AnsiString("Current_byte: ") + Current_byte );
  Debug->Lines->Add(AnsiString("Width: ") + Width );
  Debug->Lines->Add(AnsiString("bytes_Width: ") + bytes_Width );
  Debug->Lines->Add(AnsiString("bytes_Width_after_shift: ") + bytes_Width_after_shift );
  Debug->Lines->Add(AnsiString("Left_shift: ") + Left_shift );
  Debug->Lines->Add(AnsiString("Line_width: ") + Line_width );

}//inline void tDisplay::Out_debug_data_for_Line_buffer(uchar Symbol)

inline void Out_debug_data_for_Videomemory(uchar Symbol)
{

  if(!Debug)

    return;

  Debug->Lines->Add(AnsiString("  ") );
  Debug->Lines->Add(AnsiString("x0: ") + x0 );
  Debug->Lines->Add(AnsiString("y0: ") + y0 );
  Debug->Lines->Add(AnsiString("x1: ") + x1 );
  Debug->Lines->Add(AnsiString("y1: ") + y1 );
  Debug->Lines->Add(AnsiString("x: ") + x_rel );
  Debug->Lines->Add(AnsiString("y: ") + y_rel );
  Debug->Lines->Add(AnsiString("x_shift: ") + x_shift );
  Debug->Lines->Add(AnsiString("y_shift: ") + y_shift );
  Debug->Lines->Add(AnsiString("Left_shift: ") + Left_shift );
  Debug->Lines->Add(AnsiString("Current_shift: ") + Current_shift );
  Debug->Lines->Add(AnsiString("Current_byte: ") + Current_byte );
  Debug->Lines->Add(AnsiString("x_byte: ") + x_byte );
  Debug->Lines->Add(AnsiString("Start_shift: ") + Start_shift );

}//inline void tDisplay::Out_debug_data_for_Videomemory(uchar Symbol)


};// class tDisplay


#endif