#include "Display.h"

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/



const u1x tDisplay::Masks_array__left[8]=\
{0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe};

const u1x tDisplay::Masks_array__right[8]=\
{0xff,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01};

const u1x tDisplay::Masks_array__left_for_line_buffer[8]=\
{0xff,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01};

const u1x tDisplay::Masks_array__right_for_line_buffer[8]=\
{0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
tDisplay::tDisplay( uint X_size, uint Display_Y_size, uint Line_buffer_Y_size)
:Symbol_buffer( bytes_Width = (X_size + 7) >> 3 ),
Videomemory(X_size, Display_Y_size)
{

  tShift_register Line_1(bytes_Width);
        
  Line_buffer.resize (Line_buffer_Y_size,Line_1);

  x_max = X_size;
  y_max = Display_Y_size;

  Current_font = &Font6x8;

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void tDisplay::Clear_line_buffer()
{

  for(int y = 0; y < Line_buffer.size(); y ++)
  {

    Line_buffer[y].Clear();

  }

};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void tDisplay::Out_symbol()
{

  for(int Current_line = Start_line; Current_line <= End_line; Current_line++)
  {

    Symbol_buffer.Clear();

    ////////////////////////
    // ��� ����� 2 � 3 ������������ ���� ����� Out_symbol, ��� ���������� ����� ���� �������
    if(Left_shift)// ��� 2
    {

      // ���� ����� ������ ��� 8 ��� ����� ����� �������� ��� ������� �� ����� ����
      int Start_symbol_byte = Left_shift >> 3;

      Symbol_buffer.Load(0,Symbol_array + bytes_Width * Current_line + Start_symbol_byte, bytes_Width - Start_symbol_byte);

      Symbol_buffer.Shift (0, bytes_Width_after_shift - Start_symbol_byte,   Current_shift - (Left_shift & 7)  );

      Symbol_buffer[0] &= Masks_array__left_for_line_buffer[ Current_shift ];

      Line_buffer[Current_line].Or(Current_byte, &Symbol_buffer[0], bytes_Width_after_shift - Start_symbol_byte);

      continue;

    }
    else //// ��� 2
    {

      Symbol_buffer.Load(0,Symbol_array + bytes_Width * Current_line, bytes_Width);

      ////////////////////////
      Symbol_buffer.Shift(0, bytes_Width_after_shift, Current_shift);

      Line_buffer[Current_line].Or(Current_byte, &Symbol_buffer[0], bytes_Width_after_shift );

    }

  }// for(int Current_line = Start_line, Current_line <= End_line, Current_line++)

}// void tDisplay::Out_symbol ()





////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void tDisplay::Out_text_block ()
{

  Clear_line_buffer();

////////////////////////////////////////
// ����� �� ��� �������� � ��������� ���������
  Type_1:

  // ���� �� ����� ������
  while(Text < Text_end)
  {

    Width = Current_font->Width_for(*Text);
      
    // ������������ ������� ���� �� ������ � ������� �����������
    if(Left_shift >= Width)
    {
      Left_shift  -= Width;
      Text++;
    }
    else

      goto Type_2;

  }// while(Text < Text_end)

  // ����� ������
  return;

////////////////////////////////////////
Type_2:

  // ��������������
  Current_byte  = Current_shift >> 3;
  Current_shift = Current_shift & 7;

  Symbol_array = Current_font->Image_for(*Text);
  bytes_Width  = (Width + 7) >> 3;
  bytes_Width_after_shift  = (Width + Current_shift + 7) >> 3;

  Line_width     -= (Width - Left_shift);

  // �������� �������?
  if(Line_width <= 0)
  {

    Width -= Left_shift;
    
    goto Type_4;

  }

Out_debug_data_for_Line_buffer(*Text);

  Out_symbol();

  // ������������ Left_shift
  Width -= Left_shift;
  Left_shift   =  0;

  // ����� ��������� ������
  Text++;

////////////////////////////////////////
Type_3:

  // ����� ������?
  while(Text < Text_end)
  {

    //�������� Current_byte, Current_shift �� �������� ��� ����������� �������.
    Current_shift += Width;
    Current_byte  += (Current_shift >> 3);
    Current_shift =  Current_shift & 0x7;

    // ������������ �� ��������� ������
    Width        = Current_font->Width_for(*Text);
    Symbol_array = Current_font->Image_for(*Text);
    bytes_Width  = (Width + 7) >> 3;
    bytes_Width_after_shift  = (Width + Current_shift + 7) >> 3;

    Line_width     -= Width;

    // �������� �������?
    if(Line_width <= 0)

      goto Type_4;

Out_debug_data_for_Line_buffer(*Text);

    Out_symbol();

    Text++;

  }// while(*Text < Text_end)

  Current_shift += Width;
  Current_byte  += (Current_shift >> 3);
  Current_shift =  Current_shift & 0x7;

  // ����� ������
  goto Finalize;

////////////////////////////////////////
Type_4:


Out_debug_data_for_Line_buffer(*Text);

  Out_symbol();

  Current_shift += (Width + Line_width);
  Current_byte  += (Current_shift >> 3);
  Current_shift =  Current_shift & 0x7;

  for(int Current_line = Start_line; Current_line <= End_line; Current_line++)
  {

    Line_buffer[Current_line][Current_byte] &= Masks_array__right_for_line_buffer[Current_shift];

  }


Finalize:

  Out_line_buffer_in_videomemory();

  return;

}// void tDisplay::Out_text_block ()



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Out_line_buffer_in_videomemory()
{

  // ����� �������� ������ ��������������� � �������� ������ �����
  if(!Current_shift)

    if(Current_byte == 0)
    {
     int OH = 2;

      return;
    }
    else

      Current_byte--;
        
  for(int Current_line = Start_line ; Current_line <= End_line; Current_line++ )
  {

    // ����� �������� ������ �������� ���������� ����� � ���������� ���
    if(Start_shift)
    {

      Videomemory.COM__Set_coordinates(x_byte,y_abs + Current_line - Start_line);
      Videomemory.COM__Set_Read_mode();
      while(Videomemory.COM__Wait());

      u1x Intersection_byte_data = Videomemory.Read();

      Intersection_byte_data &= Masks_array__left[Start_shift];

      Line_buffer[Current_line][0] |= Intersection_byte_data;

    }

    // ����� �������� ������ �������� ���������� ����� � ���������� ���
    if(Current_shift)
    {

      Videomemory.COM__Set_coordinates(x_byte + Current_byte, y_abs + Current_line - Start_line);
      Videomemory.COM__Set_Read_mode();
      while(Videomemory.COM__Wait());

      u1x Intersection_byte_data = Videomemory.Read();

      Intersection_byte_data &= Masks_array__right[Current_shift];

      Line_buffer[Current_line][Current_byte] |= Intersection_byte_data;

    }

    Videomemory.COM__Set_coordinates(x_byte, y_abs + Current_line - Start_line);
    Videomemory.COM__Set_Write_mode();
    while(Videomemory.COM__Wait());
               
    for(uint i = 0; i <= Current_byte; i++)
      Videomemory.Write(Line_buffer[Current_line][i]);

  }//for(int Current_line = Start_line ; Current_line <= End_line; Current_line++ )

}//void tDisplay::Out_line_buffer_in_videomemory()



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
bool tDisplay::Init_text_block ()
{

  x_abs = x_rel + x0 + x_shift;
  y_abs = y_rel + y0 + y_shift;

  // x
  //////////////////////////////////
  if (x_abs < x0)
  {
    Left_shift = x0 - x_abs;
    x_abs = x0;
  }
  else
  {
    Left_shift = 0;
  }

  if(x_abs >= x1)

    return false;

  x_byte =  x_abs >> 3;
  Start_shift = Current_shift = x_abs & 7;

  Line_width = x1-x_abs;

  // y
  //////////////////////////////////
  if (y_abs < y0)
  {
    Start_line = y0 - y_abs;
    y_abs = y0;
  }
  else

    Start_line = 0;

  Height = Current_font->Height();

  if(Start_line >= Height)

    return false;

  if( (Height - Start_line) < ( y1 - y_abs) )

    End_line = Height - 1;

  else

    End_line = Start_line + (y1 - y_abs) - 1;

  Clear_line_buffer();

  Out_debug_data_for_Videomemory(0);

  Text = Text_begin;

  return true;

}// bool tDisplay::Init_text_block ()


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Control_processing ()
{

  if(*Text_end == '\n')
  {

    y_rel += Height;
    x_rel = 0;

    Text_end ++;

    Text_begin = Text_end;

    return;

  }

  Text_end ++;

}// void tDisplay::Control_processing ()


///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Out_text (int arg_x0, int arg_y0,
                         int arg_x1, int arg_y1,
                         int arg_x_shift, int arg_y_shift,
                         unsigned char * argText,
                         TMemo * argDebug)
{

  x0 = arg_x0;
  x1 = arg_x1;
  y0 = arg_y0;
  y1 = arg_y1;

  x_shift = arg_x_shift;
  y_shift = arg_y_shift;

  Text = Text_begin = Text_end = argText;

  Debug = argDebug;

  if(x1 < x0)
  {
    int temp = x0;
    x0 = x1;
    x1 = temp;
  }

  if(y1 < y0)
  {
    int temp = y0;
    y0 = y1;
    y1 = temp;
  }

  if(x0 < 0)
  {
    x_shift += x0;
    x0 = 0;
  }

  if(y0 < 0)
  {
    y_shift += y0;
    y0 = 0;
  }

  if(x1 > x_max)
  {
    x1 = x_max;
  }

  if(y1 > y_max)
  {
    y1 = y_max;
  }

  x_rel = y_rel = 0;

  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  while(*Text_end)
  {

//////////////////////////////////////
state__Inside_text_block:

  while(1)
  {

    switch(*Text_end)
    {

      case '\0':
      case '\n':
        goto state__Out_text_block;

    }

    Text_end++;

  }


//////////////////////////////////////
state__Out_text_block:

  if( (Text_begin != Text_end) && Init_text_block())
  {

    // Line_width �� ����� � ��� ������� ����� ������������ �������� ������ �����.
    // ��������� ����� ��������� ��� �������� ���������� �������� �� ���������
    // �� ������ �����, � ����� ������������ ����� ����� ����������� ��������
    x_rel += Line_width;

    Out_text_block();

    x_rel -= Line_width;

  }

  Text_begin = Text_end;

//////////////////////////////////////
state__Control_processing:

  if(*Text_end == 0)
  {

    Debug = 0;

    return;

  }
  // ����� �������� � ��������� �������
  Control_processing();

  }//while(*Text_end)

}//void tDisplay::Out_text (int arg_x0, int arg_y0,




///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Draw(TCanvas * Canvas)
{

  Canvas->Pen->Color = clBlue;
  Canvas->Pen->Width = 1;

  for(int y = 0; y < Line_buffer.size(); y ++)
  {

    for(int x = 0; x < Line_buffer[y].Size(); x ++)
    {

      for(int b = 0, mask = 0x80; b < 8; b ++, mask = mask >> 1)
      {

        if(Line_buffer[y][x] & mask)

          Canvas->Brush->Color = clBlue;

        else

          Canvas->Brush->Color = clWhite;

        Canvas->Rectangle( (x*8 + b) * 4 - 1, y * 4 - 1, (x*8 + b + 1) * 4, (y + 1) * 4);

      }//for(int b = 0, mask = 1; b < 8; b ++, mask = mask << 1)


      Canvas->Pen->Color = clBlack;
      Canvas->Pen->Width = 2;

      Canvas->MoveTo((x*8) * 4 - 1, 0);
      Canvas->LineTo((x*8) * 4 - 1, Line_buffer.size()*4 - 1);

      Canvas->Pen->Color = clBlue;
      Canvas->Pen->Width = 1;

    }//for(int x = 0; x < Line_buffer[y].Size(); x ++)

  }//for(int y = 0; y < Line_buffer.size(); y ++)

}//void tDisplay::Draw(TCanvas * Canvas)

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay::Set_font(class tFont * argFont)
{

  Current_font = argFont;

}


