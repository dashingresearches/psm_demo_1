//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

#include <Graphics.hpp>


#include <Classes.hpp>
#include <SyncObjs.hpp>

#include "Display.h"


#include "Font6x8.h"
#include "Font8x10.h"
#include "Font8x16.h"
#include "Font16x24.h"


#include "Main_tab.h"
tMain_tab_process * Main_tab_process;


const class tFont * const Font_set[]=
{
  &Font6x8,
  &Font8x10,
  &Font8x16,
  &Font16x24,
};





// ��������� �������� �� ������������ �������� ���� ����������� �������,
// � ������ AnsiString ���. ���������� ���
// ����� ������������� ����� \n \t \b \v \r \f \a \\ \? \' \" � ��� �� \xhh � \0ooo ���
// hh - ����������������� ooo ������������ �����
void Load_text(AnsiString &Destination, AnsiString &Source)
{

///////////////////////////////////////////
  Destination = "";

  if(Source.Length() == 0)

    return;
///////////////////////////////////////////

  // ������� ������ ������ � ��������
  int i = 0;
  uchar Current_symbol;

  // ��� �� ���������� ������������ � ��� �������� ����������������� � ������������ �����
  uint n = 0;
  uint Radix = 16;
  // �� ����������� ��������� ���������� �������� + 1
  uchar Data[4];

  #define mNext_symbol\
  {\
    if(++i >= Source.Length())\
    {\
      return;\
    }\
    Current_symbol = Source.c_str()[i];\
  }

  #define mAdd_symbol(Data) Destination += AnsiString((const char *)&Data,1)

  Current_symbol = Source.c_str()[0];

///////////////////////////////////////////
Common_symbol:

  while(1)
  {

    if(Current_symbol == '\\')

      goto Backslash_finded;

    mAdd_symbol(Current_symbol);

    mNext_symbol;

  }//while(1)

  return;

// Common_symbol:
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
Backslash_finded:

  mNext_symbol;

  if( (Current_symbol >= '0') && (Current_symbol <= '7') )

    goto Octal_mark_finded;

  switch(Current_symbol)
  {

    case 'n':
      Current_symbol = 0xa;
      break;
    case 't':
      Current_symbol = 0x9;
      break;
    case 'b':
      Current_symbol = 0x8;
      break;
    case 'v':
      Current_symbol = 0xb;
      break;
    case 'r':
      Current_symbol = 0xd;
      break;
    case 'f':
      Current_symbol = 0xc;
      break;
    case 'a':
      Current_symbol = 0x7;
      break;
    case 'x':
      goto Hexidecimal_mark_finded;

  } //switch(Current_symbol)

  mAdd_symbol(Current_symbol);

  mNext_symbol;

  goto Common_symbol;

// Backslash_finded
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
Hexidecimal_mark_finded:


n = 0;
Radix = 16;

#define mDigits_amount 2

  if(++i >= Source.Length())
  {

    mAdd_symbol(Current_symbol);// ��������� x

    return;

  }

  Current_symbol = Source.c_str()[i];

  if(!isxdigit(Current_symbol))
  {

    Destination += 'x';

    goto Common_symbol;

  }

  for(; n < mDigits_amount; n++, i++)
  {

    if(i >= Source.Length())
    {

      Data[n] = 0;

      goto Add_digit_code;

    }//if(++i >= Source.Length())

    Current_symbol = Source.c_str()[i];

    if(isxdigit(Current_symbol))

      Data[n] = Current_symbol;

    else
    {

      Data[n] = 0;

      goto Add_digit_code;

    }

  }//for(uint n = 0; n < mDigits_amount; n++, i++)

  goto Add_digit_code;

// Hexidecimal_mark_finded:
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
Octal_mark_finded:


// ���  ���������� ��������� � ������ �����������������
n = 0;
Radix = 8;

#define mDigits_amount 3

  for(; n < mDigits_amount; n++, i++)
  {

    if(i >= Source.Length())
    {

      Data[n] = 0;

      goto Add_digit_code;

    }//if(++i >= Source.Length())

    Current_symbol = Source.c_str()[i];

    if( (Current_symbol >= '0') && (Current_symbol <= '7'))

      Data[n] = Source.c_str()[i];

    else
    {

      Data[n] = 0;

      goto Add_digit_code;

    }

  }//for(uint n = 0; n < mDigits_amount; n++, i++)

  goto Add_digit_code;

// Octal_mark_finded:
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////
Add_digit_code:

  uchar Code = strtoul(Data, 0, Radix);

  // ���� - ����� ������ �� ���������
  if(Code)

    mAdd_symbol(Code);

  else

    return;

  if(i >= Source.Length())

    return;

  Current_symbol = Source.c_str()[i];

  goto Common_symbol;

//Add_digit_code:
//////////////////////////////////////////
///////////////////////////////////////////

}



//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{

  Main_tab_process = new tMain_tab_process;

  Main_tab_process->Text = Form1->Edit1->Text;

  Main_tab_process->x0 = Form1->TrackBar1->Position;
  Main_tab_process->x1 = Form1->TrackBar4->Position;
  Main_tab_process->y0 = Form1->TrackBar2->Position;
  Main_tab_process->y1 = Form1->TrackBar5->Position;

  Main_tab_process->x_shift = Form1->TrackBar3->Position;

  Main_tab_process->y_shift = Form1->TrackBar6->Position;

  Load_text(Main_tab_process->Text, Form1->Edit1->Text);

  Form1->Memo1->Clear();

  Main_tab_process->Was_event->SetEvent();

  Form1->Label2->Caption = AnsiString("x0  ") + (Main_tab_process->x0);
  Form1->Label3->Caption = AnsiString("x1  ") + (Main_tab_process->x1);
  Form1->Label4->Caption = AnsiString("y0  ") + (Main_tab_process->y0);
  Form1->Label5->Caption = AnsiString("y1  ") + (Main_tab_process->y1);
  Form1->Label6->Caption = AnsiString("x_shift  ") + (Main_tab_process->x_shift);
  Form1->Label7->Caption = AnsiString("y_shift  ") + (Main_tab_process->y_shift);

  randomize();

}
//---------------------------------------------------------------------------






//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar1Change(TObject *Sender)
{

  Main_tab_process->x0 = Form1->TrackBar1->Position;

  Form1->Memo1->Clear();

  Form1->Label2->Caption = AnsiString("x0  ") + (Main_tab_process->x0);

  Main_tab_process->Was_event->SetEvent();

}



//---------------------------------------------------------------------------

void __fastcall TForm1::TrackBar4Change(TObject *Sender)
{

   Main_tab_process->x1 = Form1->TrackBar4->Position;

   Form1->Memo1->Clear();

   Form1->Label3->Caption = AnsiString("x1  ") + (Main_tab_process->x1);

   Main_tab_process->Was_event->SetEvent();

}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar2Change(TObject *Sender)
{

  Main_tab_process->y0 = Form1->TrackBar2->Position;

  Form1->Memo1->Clear();

  Form1->Label4->Caption = AnsiString("y0  ") + (Main_tab_process->y0);

  Main_tab_process->Was_event->SetEvent();
  
}

void __fastcall TForm1::TrackBar5Change(TObject *Sender)
{

   Main_tab_process->y1 = Form1->TrackBar5->Position;

   Form1->Memo1->Clear();

   Form1->Label5->Caption = AnsiString("y1  ") + (Main_tab_process->y1);

  Main_tab_process->Was_event->SetEvent();

}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar3Change(TObject *Sender)
{

  Main_tab_process->x_shift = Form1->TrackBar3->Position;

  Form1->Memo1->Clear();

  Form1->Label6->Caption = AnsiString("x_shift  ") + (Main_tab_process->x_shift);



  Main_tab_process->Was_event->SetEvent();
  
}


//---------------------------------------------------------------------------


void __fastcall TForm1::TrackBar6Change(TObject *Sender)
{
  Main_tab_process->y_shift = Form1->TrackBar6->Position;

  Form1->Memo1->Clear();

  Form1->Label7->Caption = AnsiString("y_shift  ") + (Main_tab_process->y_shift);

  Main_tab_process->Was_event->SetEvent();
}
//---------------------------------------------------------------------------





//---------------------------------------------------------------------------
                  
void __fastcall TForm1::Edit1KeyPress(TObject *Sender, char &Key)
{

  if(Key == 0xd)
  {

    Load_text(Main_tab_process->Text, Form1->Edit1->Text);

    Form1->Memo1->Clear();

    Main_tab_process->Was_event->SetEvent();

  }
}
//---------------------------------------------------------------------------






void __fastcall TForm1::RadioButton1Click(TObject *Sender)
{

  Main_tab_process->Was_event->SetEvent();

}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButton2Click(TObject *Sender)
{
  Main_tab_process->Was_event->SetEvent();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{

  Main_tab_process->Display.Set_font( (tFont*) Font_set[Form1->ComboBox1->ItemIndex] );
  Form1->Memo1->Clear();
  Main_tab_process->Was_event->SetEvent();

}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{

  Main_tab_process->Let_debug_information = Form1->CheckBox1->Checked;
  Form1->Memo1->Clear();
  Main_tab_process->Was_event->SetEvent();

}
//---------------------------------------------------------------------------

